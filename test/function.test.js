const functions = require('../lib/functions.js');

test('converts Celsius to Fahrenheit correctly', () => {
    expect(functions.celsiusToFahrenheit(36.6)).toBe(97.88);
});

test('converts Fahrenheit to Celsius correctly', () => {
    expect(functions.fahrenheitToCelsius(451)).toBe(232.78);
});

test('converts Celsius to Kelvin correctly', () => {
    expect(functions.celsiusToKelvin(36.6)).toBe(309.75);
});

test('converts Kelvin to Celsius correctly', () => {
    expect(functions.kelvinToCelsius(1000)).toBe(726.85);
});

test('converts Fahrenheit to Kelvin correctly', () => {
    expect(functions.fahrenheitToKelvin(1000)).toBe(810.93);
});