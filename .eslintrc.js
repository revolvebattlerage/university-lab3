module.exports = {
    env: {
        node: true,
        es6: true,
        jest: true
    },
    plugins: ['node', 'jest'],
    extends: ['eslint:recommended', 'plugin:node/recommended'],
    rules: {
        'quotes': ['error', 'single'],
        'semi': ['error', 'always'],
        'node/no-unsupported-features/es-syntax': ['error', { 'ignores': ['modules'] }],
        'jest/no-disabled-tests': 'warn',
        'jest/no-focused-tests': 'error',
        'jest/no-identical-title': 'error',
    },
};