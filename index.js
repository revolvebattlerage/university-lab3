/* eslint-disable */
const functions = require("./lib/functions.js");

const celsiusDegrees = [0, 36.6, 100];

const fahrenheitDegrees = [0, 451, 1000];

const kelvinDegrees = [0, 100, 500, 1000];

celsiusDegrees.forEach(el => {
    console.log(`${el} degrees Celsius in Fahrenheit is equal to ${functions.celsiusToFahrenheit(el)}`);
});

fahrenheitDegrees.forEach(el => {
    console.log(`${el} degrees Fahrenheit in Kelvin is equal to ${functions.celsiusToKelvin(el)}`);
});

kelvinDegrees.forEach(el => {
    console.log(`${el} degrees Kelvin in Celsius is equal to ${functions.kelvinToCelsius(el)}`);
});