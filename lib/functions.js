function celsiusToFahrenheit(celsius) {
    return round((celsius * 9/5) + 32);
}

function fahrenheitToCelsius(fahrenheit) {
    return round((fahrenheit - 32) * 5/9);
}

function fahrenheitToKelvin(fahrenheit) {
    return round((fahrenheit - 32) * 5/9 + 273.15);
}

function celsiusToKelvin(celsius) {
    return round(celsius + 273.15);
}

function kelvinToCelsius(kelvin) {
    return round(kelvin - 273.15);
}

function round(value) {
    return Number.isInteger(value) ? value : Number(value.toFixed(2));
}

module.exports = {
    celsiusToFahrenheit,
    fahrenheitToCelsius,
    celsiusToKelvin,
    kelvinToCelsius,
    fahrenheitToKelvin,
};